export type Person = {
    age: number,
    name: string,
    alive: boolean
    favoriteColor: Color,
    hight: number,
    weight: number
}

export type SimplePerson = Pick<Person, "age" | "name">

export enum Color {
    Red = "red",
    Green = "green",
    Blue = "blue"
}