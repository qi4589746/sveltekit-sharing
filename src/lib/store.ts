import { writable, type Writable } from "svelte/store";

export const WS_List: Writable<string[]> = writable([])