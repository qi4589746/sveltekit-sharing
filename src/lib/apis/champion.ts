export type Champion = {
    "version": string,
    "id": string,
    "key": string,
    "name": string,
    "title": string,
    "blurb": string,
    "info": {
        "attack": number,
        "defense": number,
        "magic": number,
        "difficulty": number
    },
    "image": {
        "full": string,
        "sprite": string,
        "group": string,
        "x": number,
        "y": number,
        "w": number,
        "h": number
    },
    "tags": Tag[],
    "partype": string,
    "stats": {
        "hp": number,
        "hpperlevel": number,
        "mp": number,
        "mpperlevel": number,
        "movespeed": number,
        "armor": number,
        "armorperlevel": number,
        "spellblock": number,
        "spellblockperlevel": number,
        "attackrange": number,
        "hpregen": number,
        "hpregenperlevel": number,
        "mpregen": number,
        "mpregenperlevel": number,
        "crit": number,
        "critperlevel": number,
        "attackdamage": number,
        "attackdamageperlevel": number,
        "attackspeedperlevel": number,
        "attackspeed": number
    }
}

enum Tag {
    Fighter = 'Fighter',
    Tank = 'Tank',
    Mage = 'Mage',
    Assassin = 'Assassin',
    Marksman = 'Marksman',
    Support = 'Support'
}

type ChampionData = {
    "type": string,
    "format": string,
    "version": string,
    "data": {
        [key: string]: Champion
    }
}

export async function getAllChampions(): Promise<ChampionData | null> {
    const url = "https://ddragon.leagueoflegends.com/cdn/14.12.1/data/zh_TW/champion.json"
    try {
        const res = await fetch(url, {
            method: 'GET'
        })
        const status = await res.status
        const data = await res.json()
        if (status === 200) {
            return data
        }
        return null
    }
    catch (e) {
        console.error(e)
        return null
    }
}